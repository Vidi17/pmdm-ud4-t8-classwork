package dam.android.vidal.u4t8database;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import dam.android.vidal.u4t8database.data.ToDoListDBManager;

public class AddTaskActivity extends AppCompatActivity {
    private EditText etTodo, etToAccomplish, etDescription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_task);

        setUI();
    }

    private void setUI(){
        etTodo = findViewById(R.id.etTodo);
        etToAccomplish = findViewById(R.id.etToAccomplish);
        etDescription = findViewById(R.id.etDescription);
    }

    public void onClick(View view){

        if (view.getId() == R.id.buttonOK) {

            if (etTodo.getText().toString().length() > 0){
                ToDoListDBManager todoListDBManager = new ToDoListDBManager(this);

                todoListDBManager.insert(etTodo.getText().toString(),
                        etToAccomplish.getText().toString(),
                        etDescription.getText().toString());
            } else {
                Toast.makeText(this, R.string.task_data_empty, Toast.LENGTH_LONG).show();
            }

            finish();
        }
    }
}