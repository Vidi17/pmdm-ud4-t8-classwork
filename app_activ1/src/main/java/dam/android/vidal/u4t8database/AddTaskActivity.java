package dam.android.vidal.u4t8database;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import dam.android.vidal.u4t8database.data.ToDoListDBManager;

public class AddTaskActivity extends AppCompatActivity {
    private EditText etTodo, etToAccomplish, etDescription;
    private Spinner spPriority, spStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_task);

        setUI();
    }

    private void setUI(){
        etTodo = findViewById(R.id.etTodo);
        etToAccomplish = findViewById(R.id.etToAccomplish);
        etDescription = findViewById(R.id.etDescription);

        // TODO Acitividad 6.1 Añadir los campos priority y status

        spPriority = findViewById(R.id.spPriority);
        ArrayAdapter<CharSequence> priorityAdapter = ArrayAdapter.createFromResource(this
                , R.array.priority, android.R.layout.simple_spinner_item);
        priorityAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spPriority.setAdapter(priorityAdapter);

        spStatus = findViewById(R.id.spStatus);
        ArrayAdapter<CharSequence> statusAdapter = ArrayAdapter.createFromResource(this
                , R.array.status, android.R.layout.simple_spinner_item);
        statusAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spStatus.setAdapter(statusAdapter);
    }

    public void onClick(View view){

        if (view.getId() == R.id.buttonOK) {

            if (etTodo.getText().toString().length() > 0){
                ToDoListDBManager todoListDBManager = new ToDoListDBManager(this);

                todoListDBManager.insert(etTodo.getText().toString(),
                        etToAccomplish.getText().toString(),
                        etDescription.getText().toString(),
                        spPriority.getSelectedItem().toString(),
                        spStatus.getSelectedItem().toString());
            } else {
                Toast.makeText(this, R.string.task_data_empty, Toast.LENGTH_LONG).show();
            }

            finish();
        }
    }
}