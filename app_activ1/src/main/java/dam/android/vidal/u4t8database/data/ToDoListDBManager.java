package dam.android.vidal.u4t8database.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

import dam.android.vidal.u4t8database.model.Task;

public class ToDoListDBManager {
    private ToDoListDBHelper toDoListDBHelper;

    public ToDoListDBManager (Context context) {
        toDoListDBHelper = ToDoListDBHelper.getInstance(context);
    }

    public void insert (String todo, String when, String description, String priority, String status) {

        SQLiteDatabase sqLiteDatabase = toDoListDBHelper.getWritableDatabase();

        if (sqLiteDatabase != null){
            ContentValues contentValue = new ContentValues();

            contentValue.put(ToDoListDBContract.Tasks.TODO, todo);
            contentValue.put(ToDoListDBContract.Tasks.TO_ACCOMPLISH, when);
            contentValue.put(ToDoListDBContract.Tasks.DESCRIPTION, description);

            // TODO Actividad 6.1 Añadir los campos priority y status a la tabla TASKS
            contentValue.put(ToDoListDBContract.Tasks.PRIORITY, priority);
            contentValue.put(ToDoListDBContract.Tasks.STATUS, status);

            sqLiteDatabase.insert(ToDoListDBContract.Tasks.TABLE_NAME, null, contentValue);

        }
    }

    public ArrayList<Task> getTasks() {
        ArrayList<Task> taskList = new ArrayList<>();

        SQLiteDatabase sqLiteDatabase = toDoListDBHelper.getReadableDatabase();

        if (sqLiteDatabase != null){
            String[] projection = new String[]{
                    ToDoListDBContract.Tasks._ID,
                    ToDoListDBContract.Tasks.TODO,
                    ToDoListDBContract.Tasks.TO_ACCOMPLISH,
                    ToDoListDBContract.Tasks.DESCRIPTION,
                    ToDoListDBContract.Tasks.PRIORITY,
                    ToDoListDBContract.Tasks.STATUS};

            Cursor cursoToDoList = sqLiteDatabase.query(ToDoListDBContract.Tasks.TABLE_NAME,
                    projection,
                    null,
                    null,
                    null,
                    null,
                    null);

            if (cursoToDoList != null) {

                int _idIndex = cursoToDoList.getColumnIndexOrThrow(ToDoListDBContract.Tasks._ID);
                int todoIndex = cursoToDoList.getColumnIndexOrThrow(ToDoListDBContract.Tasks.TODO);
                int to_accomplishIndex = cursoToDoList.getColumnIndexOrThrow(ToDoListDBContract.Tasks.TO_ACCOMPLISH);
                int descriptionIndex = cursoToDoList.getColumnIndexOrThrow(ToDoListDBContract.Tasks.DESCRIPTION);
                int priorityIndex = cursoToDoList.getColumnIndexOrThrow(ToDoListDBContract.Tasks.PRIORITY);
                int statusIndex = cursoToDoList.getColumnIndexOrThrow(ToDoListDBContract.Tasks.STATUS);


                while (cursoToDoList.moveToNext()){
                    Task task = new Task(
                            cursoToDoList.getInt(_idIndex),
                            cursoToDoList.getString(todoIndex),
                            cursoToDoList.getString(to_accomplishIndex),
                            cursoToDoList.getString(descriptionIndex),
                            cursoToDoList.getString(priorityIndex),
                            cursoToDoList.getString(statusIndex));
                    taskList.add(task);
                }

                cursoToDoList.close();
            }
        }
        return taskList;
    }

    public void close() {
        toDoListDBHelper.close();
    }

}
