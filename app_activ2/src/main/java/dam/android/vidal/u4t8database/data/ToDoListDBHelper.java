package dam.android.vidal.u4t8database.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class ToDoListDBHelper extends SQLiteOpenHelper {

    private static ToDoListDBHelper instanceDBHelper;

    public static synchronized ToDoListDBHelper getInstance(Context context) {

        if (instanceDBHelper == null) {
            instanceDBHelper = new ToDoListDBHelper(context.getApplicationContext());
        }
        return instanceDBHelper;
    }

    public ToDoListDBHelper(Context context) {
        super(context, ToDoListDBContract.DB_NAME, null, ToDoListDBContract.DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(ToDoListDBContract.Tasks.CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        sqLiteDatabase.execSQL(ToDoListDBContract.Tasks.DELETE_TABLE);

        onCreate(sqLiteDatabase);
    }
}
