package dam.android.vidal.u4t8database.model;

public class Task {
    private int _id;
    private String todo;
    private String toAccomplish;
    private String description;

    // TODO Actividad 6.1 Añadir los campos priority y status a la tabla TASK
    private String priority;
    private String status;

    public Task(int _id, String todo, String to_accomplish, String description, String priority, String status) {
        this._id = _id;
        this.todo = todo;
        this.toAccomplish = to_accomplish;
        this.description = description;
        this.priority = priority;
        this.status = status;
    }

    public int get_id() {
        return _id;
    }

    public String getTodo() {
        return todo;
    }

    public String getToAccomplish() {
        return toAccomplish;
    }

    public String getDescription() {
        return description;
    }

    public String getPriority() {
        return priority;
    }

    public String getStatus() {
        return status;
    }
}
