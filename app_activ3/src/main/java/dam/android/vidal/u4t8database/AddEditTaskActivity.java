package dam.android.vidal.u4t8database;

import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import dam.android.vidal.u4t8database.data.ToDoListDBManager;

public class AddEditTaskActivity extends AppCompatActivity {
    private EditText etTodo, etToAccomplish, etDescription;
    private Spinner spPriority, spStatus;
    private Bundle extras;
    private String id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_task);

        setUI();
        extras = getIntent().getExtras();
        if (extras != null){
            setValues(extras);
        }
    }

    // TODO Actividad 6.2 Recoger los valores de una tarea ya creada para editarla
    private void setValues(Bundle extras) {
        id = extras.getString("Id");
        etTodo.setText(extras.getString("Todo"));
        etToAccomplish.setText(extras.getString("ToAccomplish"));
        etDescription.setText(extras.getString("Description"));
        spPriority.setSelection(translateArrays(extras.getString("Priority")));
        spStatus.setSelection(translateArrays(extras.getString("Status")));
    }

    private int translateArrays(String item){
        switch (item){
            case "Low":
            case "Not Started":
                return 0;
            case "Normal":
            case "In Progress":
                return 1;
            default:
                return 2;
        }
    }

    private void setUI(){
        etTodo = findViewById(R.id.etTodo);
        etToAccomplish = findViewById(R.id.etToAccomplish);
        etDescription = findViewById(R.id.etDescription);

        // TODO Acitividad 6.1 Añadir los campos priority y status

        spPriority = findViewById(R.id.spPriority);
        ArrayAdapter<CharSequence> priorityAdapter = ArrayAdapter.createFromResource(this
                , R.array.priority, android.R.layout.simple_spinner_item);
        priorityAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spPriority.setAdapter(priorityAdapter);

        spStatus = findViewById(R.id.spStatus);
        ArrayAdapter<CharSequence> statusAdapter = ArrayAdapter.createFromResource(this
                , R.array.status, android.R.layout.simple_spinner_item);
        statusAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spStatus.setAdapter(statusAdapter);
    }

    // TODO Actividad 6.2 Implementación de los metodos on click según el botón sea save o delete
    public void onClick(View view){

        if (extras != null && view.getId() == R.id.btSave){
            ToDoListDBManager todoListDBManager = new ToDoListDBManager(this);

            todoListDBManager.update(id, etTodo.getText().toString(),
                    etToAccomplish.getText().toString(),
                    etDescription.getText().toString(),
                    spPriority.getSelectedItem().toString(),
                    spStatus.getSelectedItem().toString());
            finish();
        } else if (view.getId() == R.id.btSave) {

            if (etTodo.getText().toString().length() > 0){
                ToDoListDBManager todoListDBManager = new ToDoListDBManager(this);

                todoListDBManager.insert(etTodo.getText().toString(),
                        etToAccomplish.getText().toString(),
                        etDescription.getText().toString(),
                        spPriority.getSelectedItem().toString(),
                        spStatus.getSelectedItem().toString());
            } else {
                Toast.makeText(this, R.string.task_data_empty, Toast.LENGTH_LONG).show();
            }

            finish();
        } else if (view.getId() == R.id.btDelete){
            ToDoListDBManager todoListDBManager = new ToDoListDBManager(this);

            todoListDBManager.delete(id);
            finish();
        }
    }
}