package dam.android.vidal.u4t8database;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import dam.android.vidal.u4t8database.data.ToDoListDBManager;

public class MainActivity extends AppCompatActivity {

    private RecyclerView rvTodoList;
    private ToDoListDBManager todoListDBManager;
    private MyAdapter myAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        todoListDBManager = new ToDoListDBManager(this);
        myAdapter = new MyAdapter(todoListDBManager);

        setUI();
    }

    private void setUI(){

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(v -> startActivity(new Intent(getApplicationContext(), AddEditTaskActivity.class)));

        rvTodoList = findViewById(R.id.rvTodoList);
        rvTodoList.setHasFixedSize(true);
        rvTodoList.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        rvTodoList.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        rvTodoList.setAdapter(myAdapter);
    }

    @Override
    public void onResume() {
        super.onResume();

        myAdapter.getData();
    }

    @Override
    public void onDestroy(){
        todoListDBManager.close();

        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.options, menu);
        return true;
    }



    // TODO Actividad 6.3 Opciones de la AppBar con funcionalidad
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.option_showNotStarted:
                myAdapter.getFilteredData("Not Started");
                break;
            case R.id.option_showInProgress:
                myAdapter.getFilteredData("In Progress");
                break;
            case R.id.option_showCompleted:
                myAdapter.getFilteredData("Completed");
                break;
            case R.id.option_showAll:
                myAdapter.getData();
                break;
            case R.id.option_deleteCompletedTasks:
                todoListDBManager.deleteCompletedTasks();
                myAdapter.getData();
                break;
            case R.id.option_deleteAllTasks:
                todoListDBManager.deleteAllTasks();
                myAdapter.getData();
                break;
        }
        return true;
    }
}