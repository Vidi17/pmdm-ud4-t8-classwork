package dam.android.vidal.u4t8database;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import org.w3c.dom.Text;

import java.util.ArrayList;

import dam.android.vidal.u4t8database.data.ToDoListDBManager;
import dam.android.vidal.u4t8database.model.Task;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder>{
    private ToDoListDBManager todoListDBManager;
    private ArrayList<Task> myTaskList;

    static class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvId;
        TextView tvTodo;
        TextView tvToAccomplish;
        TextView tvDescription;
        TextView tvPrioritySelected;
        TextView tvStatus;

        public MyViewHolder(View view) {
            super(view);

            this.tvId = view.findViewById(R.id.tvId);
            this.tvTodo = view.findViewById(R.id.tvTodo);
            this.tvToAccomplish = view.findViewById(R.id.tvToAccomplish);
            this.tvDescription = view.findViewById(R.id.tvDescription);
            this.tvPrioritySelected = view.findViewById(R.id.tv_PrioritySelected);
            this.tvStatus = view.findViewById(R.id.tvStatus);
            view.setOnClickListener(v -> {
                Intent intent = new Intent(v.getContext(), AddEditTaskActivity.class);
                intent.putExtra("Id", tvId.getText());
                intent.putExtra("Todo", tvTodo.getText());
                intent.putExtra("ToAccomplish", tvToAccomplish.getText());
                intent.putExtra("Description", tvDescription.getText());
                intent.putExtra("Priority", tvPrioritySelected.getText());
                intent.putExtra("Status", tvStatus.getText());
                v.getContext().startActivity(intent);
            });
        }

        public void bind (Task task){
            this.tvId.setText(String.valueOf(task.get_id()));
            this.tvTodo.setText(task.getTodo());
            this.tvToAccomplish.setText(task.getToAccomplish());
            this.tvDescription.setText(task.getDescription());
            this.tvPrioritySelected.setText(task.getPriority());
            this.tvStatus.setText(task.getStatus());
        }
    }

    public MyAdapter (ToDoListDBManager todoListDBManager) {
        this.todoListDBManager = todoListDBManager;
    }

    public void getData() {
        this.myTaskList = todoListDBManager.getTasks();
        notifyDataSetChanged();
    }

    // TODO Actividad 6.3 Recoger los datos filtrados
    public void getFilteredData(String filter) {
        this.myTaskList = todoListDBManager.getStatusFilteredTasks(filter);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemLayout = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_layout, parent, false);

        return new MyViewHolder(itemLayout);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder viewHolder, int position) {

        viewHolder.bind(myTaskList.get(position));
    }

    @Override
    public int getItemCount() {
        return  myTaskList.size();
    }
}
